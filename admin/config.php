<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/opencart/admin/');
define('HTTP_CATALOG', 'http://localhost/opencart/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/opencart/admin/');
define('HTTPS_CATALOG', 'http://localhost/opencart/');

// DIR
define('DIR_APPLICATION', 'D:/Work/XAMPP/php/www/OpenCart/admin/');
define('DIR_SYSTEM', 'D:/Work/XAMPP/php/www/OpenCart/system/');
define('DIR_IMAGE', 'D:/Work/XAMPP/php/www/OpenCart/image/');
define('DIR_LANGUAGE', 'D:/Work/XAMPP/php/www/OpenCart/admin/language/');
define('DIR_TEMPLATE', 'D:/Work/XAMPP/php/www/OpenCart/admin/view/template/');
define('DIR_CONFIG', 'D:/Work/XAMPP/php/www/OpenCart/system/config/');
define('DIR_CACHE', 'D:/Work/XAMPP/php/www/OpenCart/system/storage/cache/');
define('DIR_DOWNLOAD', 'D:/Work/XAMPP/php/www/OpenCart/system/storage/download/');
define('DIR_LOGS', 'D:/Work/XAMPP/php/www/OpenCart/system/storage/logs/');
define('DIR_MODIFICATION', 'D:/Work/XAMPP/php/www/OpenCart/system/storage/modification/');
define('DIR_UPLOAD', 'D:/Work/XAMPP/php/www/OpenCart/system/storage/upload/');
define('DIR_CATALOG', 'D:/Work/XAMPP/php/www/OpenCart/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'OpenCart');
define('DB_PORT', '3306');
define('DB_PREFIX', '');
