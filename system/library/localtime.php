<?php
class Localtime
{
    public function __construct($directory = '') {
        $this->directory = $directory;
    }

    public function get($lang) {
        setlocale(LC_TIME, $lang);
        return (strftime("%d %B %G"));
    }
}